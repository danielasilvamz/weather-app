//logica de conexion al api
//siempre se coloca lo de linea 2 para inciar funcion y dentro de {} pongo logica
$(document).ready(function(){
    //cuando de click en elemento con id submitCity (el boton) vaya a funcion 
    $("#submitCity").click(function(){
        return getWeather();
    });
    
    
});


function getWeather(){

    // atraparle el valor de la
    // ciudad que digito


    var city = $("#city").val();

    if(city != '')
     // que haga la consulta si es diferente de vacio
    {
       // aqui se hace la logica para 
       // contactar el servicio web


       // Ajax se utiliza para contactar
       // el servicio web
       
       $.ajax({
       
//https://api.openweathermap.org/data/2.5/weather?q=bogota&units=metric&APPID=c10bb3bd22f90d636baa008b1529ee25
        url: 'https://api.openweathermap.org/data/2.5/weather?q=' + city + "&units=metric" + "&APPID=c10bb3bd22f90d636baa008b1529ee25",
        type: "GET",
        dataType: "jsonp",
        success: function(data){
            var widget = showResults(data)
            //data es el json retornado 
            
            
            $("#showWeather").html(widget);
            
            $("#city").val('');
        }
        
    });
    
    
}else{
    $("#error").html("<div class='alert alert-danger' id='errorCity'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>City field cannot be empty</div>");
}


}


function showResults(data){
return  '<h2 style="font-weight:bold; font-size:30px; padding-top:20px;" class="text-center">Current Weather for '+data.name+', '+data.sys.country+'</h2>'+
        "<h3 style='padding-left:40px;'><strong>Weather</strong>: "+data.weather[0].main+"</h3>"+
        "<h3 style='padding-left:40px;'><strong>Description</strong>:<img src='https://openweathermap.org/img/w/"+data.weather[0].icon+".png'> "+data.weather[0].description+"</h3>"+
        "<h3 style='padding-left:40px;'><strong>Temperature</strong>: "+data.main.temp+" &deg;C</h3>"+
        "<h3 style='padding-left:40px;'><strong>Pressure</strong>: "+data.main.pressure+" hpa</h3>"+
        "<h3 style='padding-left:40px;'><strong>Humidity</strong>: "+data.main.humidity+"%</h3>"+
        "<h3 style='padding-left:40px;'><strong>Min Temperature</strong>: "+data.main.temp_min+"&deg;C</h3>"+
        "<h3 style='padding-left:40px;'><strong>Max Temperature</strong>: "+data.main.temp_max+"&deg;C</h3>"+
        "<h3 style='padding-left:40px;'><strong>Wind Speed</strong>: "+data.wind.speed+"m/s</h3>"+
        "<h3 style='padding-left:40px; padding-bottom:30px;'><strong>Wind Direction</strong>: "+data.wind.deg+"&deg;</h3>";
}
